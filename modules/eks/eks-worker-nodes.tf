#
# EKS Worker Nodes Resources
#  * IAM role allowing Kubernetes actions to access other AWS services
#  * EKS Node Group to launch worker nodes
#

resource "aws_iam_role" "node" {
  name = "${var.cluster-name}-node"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "node-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.node.name
}

resource "aws_iam_role_policy_attachment" "node-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.node.name
}

resource "aws_iam_role_policy_attachment" "node-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.node.name
}

resource "aws_eks_node_group" "eksnode" {
  cluster_name    = "${var.name}"
  node_group_name = "${var.nodegroup_name}"
  node_role_arn   = aws_iam_role.node.arn
  subnet_ids      = "${var.subnet_id}"
  instance_types  =  ["${var.instance_type}"]
  #instance_types  =  ["c4.2xlarge"]
  disk_size       = "${var.instance_disk_size}"
  remote_access { 
    ec2_ssh_key = "${var.instance_ssh_key}"
  }
  scaling_config {
    desired_size = "${var.eks_desired_size}"
    max_size     = "${var.eks_max_size}"
    min_size     = "${var.eks_min_size}"
  }

  depends_on = [
    aws_eks_cluster.cluster,   
    aws_iam_role_policy_attachment.node-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.node-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.node-AmazonEC2ContainerRegistryReadOnly,
  ]
}
