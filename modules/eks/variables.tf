variable "aws_region" {
  default = "us-west-2"
}

variable "cluster-name" {
  default = "terraform-eks-demo"
  type    = string
}


variable "nodegroup_name" {
  default = "dev"
  type    = string
}


variable "subnet_id" {
  type    = list
}


variable "vpc-id" {
  type    = string
}


variable "name" {
  default = "name"
}

variable "instance_type" {
  default = "t3.medium"

}

variable "instance_disk_size" {
  
  default = 20

}

variable "instance_ssh_key" {
 
 default = "dev"

}

variable "eks_desired_size" {
  
  default = "3"

}

variable "eks_min_size" {

   default = "3" 

}

variable "eks_max_size" {
  
   default = "5"

}
