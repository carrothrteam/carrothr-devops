output "VPC_ID" {
   value = aws_vpc.main.*.id
}

output "VPC_CIDR" {

   value = aws_vpc.main.*.cidr_block
}

output "private_subnet_a_ids" {
  value       = aws_subnet.private_a.*.id
}
output "private_subnet_b_ids" {
  value       = aws_subnet.private_b.*.id
}
output "private_subnets" {
  value       = flatten([compact(aws_subnet.private_a.*.id), compact(aws_subnet.private_b.*.id)])
}
output "private_subnet_route_tables" {
  description = "List of IDs of private subnets"
  value       = flatten([aws_route_table.private_a.*.id, aws_route_table.private_b.*.id])
}

output "public_subnet_ids" {
  description = "List of IDs of privateB subnets"
  value       = aws_subnet.public_a.*.id
}

output "nat_eips" {
  description = "NAT IP addresses"
  value       = try(aws_eip.nat[*].public_ip, "")
}

output "private_subnet_1a_cidr" {
  description = " Private subnet 1A CIDR in Availability Zone 1"
  value       = try(aws_subnet.private_a[0].cidr_block, "")
}

output "private_subnet_1a_id" {
  description = " Private subnet 1A ID in Availability Zone 1"
  value       = try(aws_subnet.private_a[0].id, "")
}

output "private_subnet_1b_cidr" {
  description = " Private subnet 1B CIDR in Availability Zone 1"
  value       = try(aws_subnet.private_b[0].cidr_block, "")
}

output "private_subnet_1b_id" {
  description = " Private subnet 1B ID in Availability Zone 1"
  value       = try(aws_subnet.private_b[0].id, "")
}


