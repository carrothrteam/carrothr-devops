######
# VPC
######

data "aws_availability_zones" "available" {}

locals {
  availability_zones     = var.availability_zones == null ? data.aws_availability_zones.available.names : var.availability_zones
}

resource "aws_vpc" "main" {
  count = "${var.vpc_count}"
  cidr_block           = "${var.vpc_cidr}"
  #instance_tenancy     = "${var.instance_tenancy}"
  enable_dns_hostnames = "${var.enable_dns_hostnameas}"
  enable_dns_support   = "${var.enable_dns_support}"

}

###################
# Internet Gateway
###################
resource "aws_internet_gateway" "gw" {
  count  = var.igw_count
  vpc_id = aws_vpc.main[count.index].id


}

################
# Publiс routes
################
resource "aws_route_table" "public_a" {
  count  = "${var.public_route_table_count}"
  vpc_id = aws_vpc.main[count.index].id


}

resource "aws_route" "public_internet_gateway_a" {
  count                  = "${var.igw_count}"
  route_table_id         = aws_route_table.public_a[count.index].id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.gw[count.index].id

  timeouts {
    create = "5m"
  }
}



resource "aws_route_table" "public_b" {
  count  = "${var.public_route_table_count}"
  vpc_id = aws_vpc.main[count.index].id


}

resource "aws_route" "public_internet_gateway_b" {
  count                  = "${var.igw_count}"
  route_table_id         = aws_route_table.public_b[count.index].id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.gw[count.index].id

  timeouts {
    create = "5m"
  }
}


#################
# Private routes A
# We always create one route table per subnet, regardless of how many nat gateways are deployed
#################
resource "aws_route_table" "private_a" {
  count  = "${var.private_subnet_a_count}"
  vpc_id = aws_vpc.main[0].id
}

#################
# Private routes B
# We always create one route table per subnet, regardless of how many nat gateways are deployed
#################
resource "aws_route_table" "private_b" {
  count  = "${var.private_subnet_a_count}"
  vpc_id = aws_vpc.main[0].id
}




################
# Public subnet a
################
resource "aws_subnet" "public_a" {
  count                   = "${var.public_subnet_a_count}"
  vpc_id                  = aws_vpc.main[0].id
  cidr_block              = "${var.public_subnet_a_cidrs}"
  availability_zone = "${var.public_a_zone}"
  map_public_ip_on_launch = true
  tags = {
      "kubernetes.io/cluster/prod-cluster" = "shared"
       "kubernetes.io/role/elb"             = "1"
   }


}

################
# Public subnet b
################
resource "aws_subnet" "public_b" {
  count                   = "${var.public_subnet_b_count}"
  vpc_id                  = aws_vpc.main[0].id
  cidr_block              = "${var.public_subnet_b_cidrs}"
  availability_zone = "${var.public_b_zone}"
  map_public_ip_on_launch = true
  tags = {
      "kubernetes.io/cluster/prod-cluster" = "shared"
       "kubernetes.io/role/elb"             = "1"
  }

}


#################
# Private subnet A
#################
resource "aws_subnet" "private_a" {
  count             = "${var.private_subnet_a_count}"
  vpc_id            = aws_vpc.main[0].id
  cidr_block        = "${var.private_subnet_a_cidrs}"
  availability_zone = "${var.private_a_zone}"

}

#################
# Private subnet B
#################
resource "aws_subnet" "private_b" {
  count             = "${var.private_subnet_b_count}"
  vpc_id            = aws_vpc.main[0].id
  cidr_block        = "${var.private_subnet_b_cidrs}"
  availability_zone = "${var.private_b_zone}"
}


##############
# NAT Gateway
##############

resource "aws_eip" "nat" {
  count = "${var.nat_gateway_private_a_count}"
  vpc   = true

  tags = {
    Name = "${var.name}_EIP_a_nat_${count.index}"
  }
}

resource "aws_nat_gateway" "nat_gw" {
  count         = "${var.nat_gateway_private_a_count}"
  allocation_id = aws_eip.nat[count.index].id
  subnet_id     = aws_subnet.public_a[count.index].id
  tags = {
    Name = "${var.name}_EIP_a_nat_gateway_${count.index}"
  }
  depends_on = [aws_internet_gateway.gw]
}

resource "aws_route" "private_a_nat_gateway" {
  count                  = "${var.nat_gateway_private_a_count}"
  route_table_id         = aws_route_table.private_a[count.index].id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.nat_gw[count.index].id

  timeouts {
    create = "5m"
  }
}

resource "aws_route" "private_b_nat_gateway" {
  count                  = "${var.nat_gateway_private_b_count}"
  route_table_id         = aws_route_table.private_b[count.index].id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.nat_gw[count.index].id

  timeouts {
    create = "5m"
  }
}


##########################
# Route table association
##########################
resource "aws_route_table_association" "private_a" {
  count          = "${var.private_subnet_a_count}"
  subnet_id      = aws_subnet.private_a[count.index].id
  route_table_id = aws_route_table.private_a[count.index].id
}

resource "aws_route_table_association" "private_b" {
  count          = "${var.private_subnet_b_count}"
  subnet_id      = aws_subnet.private_b[count.index].id
  route_table_id = aws_route_table.private_b[count.index].id
}

resource "aws_route_table_association" "public_a" {
  count          = "${var.public_subnet_a_count}"
  subnet_id      = aws_subnet.public_a[count.index].id
  route_table_id = aws_route_table.public_a[count.index].id
}

resource "aws_route_table_association" "public_b" {
  count          = "${var.public_subnet_b_count}"
  subnet_id      = aws_subnet.public_b[count.index].id
  route_table_id = aws_route_table.public_b[count.index].id
}



