variable "name" {
   default  =  "dev"
}
variable "vpc_cidr" {
  default = "192.168.0.0/16"
}
variable "vpc_count" {
  default  = 1
}

variable "instance_tenancy" {
 default  = "dedicated"
}

variable "enable_dns_hostnameas" {
 default  = "true"
}
variable "enable_dns_support" {
 default = "ture"
} 
variable "vpc_tags" {
  default = "dev-env"
}

variable "igw_count" {
 default  = 1
}
variable "public_route_table_count" {
 default = 1
}
variable "private_subnet_a_count" { 

  default  = 1
}

variable "public_subnet_a_count" { 
  default  = 1
}

variable "public_subnet_b_count" { 
  default  = 1
}
variable "public_subnet_a_cidrs"  { 
  default  = "192.168.1.0/24"
}

variable "public_subnet_b_cidrs"  { 
  default  = "192.168.2.0/24"
}
variable "private_subnet_a_cidrs" {
 default  = "192.168.2.0/24" 
}
variable "private_subnet_b_count" { 
  default  = 1
}
variable "private_subnet_b_cidrs" { 
 default  = "192.168.3.0/24"
}
variable "nat_gateway_private_a_count" { 
 default  = 1
}
variable "nat_gateway_private_b_count" {
default  =  1

}
variable "availability_zones" {
  description = "A list of availability zones to use for subnets. If this is not provided availability zones for subnets will be automatically selected"
  type        = list(string)
  default     = null
}

variable "private_b_zone" {
   type = string
}

variable "private_a_zone" {
   type = string
}

variable "public_a_zone" {
   type = string
}

variable "public_b_zone" {
   type = string
}
