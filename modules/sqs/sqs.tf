resource "aws_sqs_queue" "prod_sqs_standard" {
   count = "${length(var.sqs_standard_names)}"
   name = "${var.sqs_standard_names[count.index]}"

}


resource "aws_sqs_queue" "prod_sqs_fifo" {

   count         = "${length(var.sqs_fifo_names)}"
   name          = "${var.sqs_fifo_names[count.index]}"
   fifo_queue    = true 
   deduplication_scope   = "queue"
   fifo_throughput_limit = "perQueue"
}
