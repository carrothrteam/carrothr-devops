variable "sqs_standard_names" {
  type = list
  default = ["dev.app", "uat.app", "prod.app"]
}

variable "sqs_fifo_names" {
  type = list
  default = ["dev.app", "uat.app", "prod.app"]
}

