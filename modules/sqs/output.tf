output "sqs_id_fifo"  {

    value  = aws_sqs_queue.prod_sqs_fifo.*.id
}

output "sqs_arn_fifo"  { 

   value = aws_sqs_queue.prod_sqs_fifo.*.arn

}

output "sqs_id_standard"  {

    value  = aws_sqs_queue.prod_sqs_standard.*.id
}

output "sqs_arn_standard"  { 

   value = aws_sqs_queue.prod_sqs_standard.*.arn

}
