resource "aws_ecr_repository" "foo" {
  name                 = "${var.reponame}"

  image_scanning_configuration {
    scan_on_push = true
  }
}

output "repo_url" {
    value = aws_ecr_repository.foo.repository_url
}

output "repo_id" {

    value = aws_ecr_repository.foo.registry_id
}


