resource "tls_private_key" "japrod" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "jakp" {
  key_name   = "ja-prod"       # Create "japrod" to AWS!!
  public_key = tls_private_key.japrod.public_key_openssh

  provisioner "local-exec" { # Create "japrod.pem" to your computer!!
    command = "echo '${tls_private_key.japrod.private_key_pem}' > ./japrod.pem"
  }
}
