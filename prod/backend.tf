terraform {
  backend "s3" {
    bucket         = "ja-prod-terraform-backend"
    key            = "ecs-platform"
    region         = "us-west-1"
  }
}
