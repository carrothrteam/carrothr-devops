module "ja_vpc" {

   source = "../modules/vpc"
   name = "prod"
   vpc_count = 1
   vpc_cidr = "172.16.0.0/16"
   instance_tenancy = "dedicated"
   enable_dns_hostnameas = true
   enable_dns_support = true
   vpc_tags = "prod-env"
   igw_count = 1
   public_route_table_count = 1
   public_subnet_a_count = 1
   public_subnet_a_cidrs = "172.16.5.0/24"

   public_subnet_b_count = 1
   public_subnet_b_cidrs = "172.16.2.0/24"
   private_subnet_a_count = 1
   private_subnet_a_cidrs = "172.16.3.0/24"
   private_subnet_b_count = 1
   private_subnet_b_cidrs = "172.16.4.0/24"
   nat_gateway_private_a_count = 1
   nat_gateway_private_b_count = 1
   private_a_zone = "us-east-1a"
   private_b_zone = "us-east-1b"
   public_a_zone = "us-east-1a"
   public_b_zone = "us-east-1b"
}


module "ja_prod_cluster" {

  source = "../modules/eks"
  name = "apiprod-cluster"
  cluster-name = "apiprod-cluster"
  nodegroup_name = "apiprod-node"
  vpc-id = "${module.ja_vpc.VPC_ID[0]}"
  subnet_id = ["${module.ja_vpc.private_subnet_1b_id}", "${module.ja_vpc.private_subnet_1a_id}" ]
  instance_type = "m5.4xlarge"
  instance_disk_size = 100
  instance_ssh_key = "ja-prod"
  eks_desired_size = 3
  eks_min_size = 3 
  eks_max_size = 5 

}

module "ja_externalapi_prod_cluster" {

  source = "../modules/eks"
  name = "externalapiprod-cluster"
  cluster-name = "externalapiprod-cluster"
  nodegroup_name = "externalapiprod-node"
  vpc-id = "${module.ja_vpc.VPC_ID[0]}"
  subnet_id = ["${module.ja_vpc.private_subnet_1b_id}", "${module.ja_vpc.private_subnet_1a_id}" ]
  instance_type = "m5.4xlarge"
  instance_disk_size = 100
  instance_ssh_key = "ja-prod"
  eks_desired_size = 2
  eks_min_size = 2
  eks_max_size = 5

}

module "ja_prod_repo" {

   source = "../modules/ecr"
   reponame = "prod-eks"

}

module "ja_prod_acm" {
   source = "../modules/acm/"
   domainname = "joinassembly.com"
   aliasdomainname = "*.joinassembly.com"

}

module "ja_prod_s3" {
   source = "../modules/s3"
   s3_bucket_names = [ "prod-assembly-kubeflows", "prod-assembly-kubeflow-thumbnails", "prod-assembly-kubetemp-download-files" ]

}

module "ja_prod_sqs" {
     source = "../modules/sqs"
     sqs_standard_names = [ "ASSEMBLY-BUS-PRODUCTION-ADP-BILLING", "ASSEMBLY-BUS-PRODUCTION-ANNIVERSARY-RECOGNITIONS",  "ASSEMBLY-BUS-PRODUCTION-BIRTHDAY-RECOGNITIONS", "ASSEMBLY-BUS-PRODUCTION-CREATE-MEMBER-INVITES",  "ASSEMBLY-BUS-PRODUCTION-IM-USER-SYNC", "ASSEMBLY-BUS-PRODUCTION-SEND-INVITE-REMINDERS" ]
     sqs_fifo_names  = [ "ASSEMBLY-BUS-PRODUCTION-ANNIVERSARY-RECOGNITIONS.fifo", "ASSEMBLY-BUS-PRODUCTION-BIRTHDAY-RECOGNITIONS.fifo", "ASSEMBLY-BUS-PRODUCTION-CREATE-MEMBER-INVITES.fifo", "ASSEMBLY-BUS-PRODUCTION-IM-USER-SYNC.fifo", "ASSEMBLY-BUS-PRODUCTION-SEND-INVITE-REMINDERS.fifo" ]

}
resource "aws_iam_policy" "ALBIngressPolicy" {
    name = "ALBIngressPolicies-prod"
    policy = "${file("albiampolicy.json")}"
}

output "EKSArn" {
   value = aws_iam_policy.ALBIngressPolicy.arn 
 }
