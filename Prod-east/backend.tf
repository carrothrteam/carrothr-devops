terraform {
  backend "s3" {
    bucket         = "ja-prod-terraform-backend-eu"
    key            = "ecs-platform"
    region         = "us-east-1"
  }
}
