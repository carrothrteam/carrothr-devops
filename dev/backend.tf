terraform {
  backend "s3" {
    bucket         = "ja-dev-terraform-backend"
    key            = "ecs-platform"
    region         = "us-west-1"
  }
}
