output "ecr_repo_url" {
    value = "${module.ja_dev_repo.repo_url}"
}

output "ecr_repo_id" {

    value = "${module.ja_dev_repo.repo_id}"
}

output "cluster_endpoint" {

    value = "${module.ja_dev_cluster.cluster_endpoint}"

}

output "acm_certificate_arn" { 
     value = "${module.ja_dev_acm.certarn}"
 #    sensitive = false

}
