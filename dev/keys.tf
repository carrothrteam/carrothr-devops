resource "tls_private_key" "jadev" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "jakp" {
  key_name   = "ja-dev"       # Create "jadev" to AWS!!
  public_key = tls_private_key.jadev.public_key_openssh

  provisioner "local-exec" { # Create "jadev.pem" to your computer!!
    command = "echo '${tls_private_key.jadev.private_key_pem}' > ./jadev.pem"
  }
}
