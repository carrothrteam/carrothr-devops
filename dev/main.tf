module "ja_vpc" {

   source = "../modules/vpc"
   name = "dev"
   vpc_count = 1
   vpc_cidr = "172.16.0.0/16"
   instance_tenancy = "dedicated"
   enable_dns_hostnameas = true
   enable_dns_support = true
   vpc_tags = "dev-env"
   igw_count = 1
   public_route_table_count = 1
   public_subnet_a_count = 1
   public_subnet_a_cidrs = "172.16.5.0/24"

   public_subnet_b_count = 1
   public_subnet_b_cidrs = "172.16.2.0/24"
   private_subnet_a_count = 1
   private_subnet_a_cidrs = "172.16.3.0/24"
   private_subnet_b_count = 1
   private_subnet_b_cidrs = "172.16.4.0/24"
   nat_gateway_private_a_count = 1
   nat_gateway_private_b_count = 1
   private_a_zone = "us-west-1a"
   private_b_zone = "us-west-1b"
   public_a_zone = "us-west-1a"
   public_b_zone = "us-west-1b"
}


module "ja_dev_cluster" {

  source = "../modules/eks"
  name = "dev-cluster"
  cluster-name = "dev-cluster"
  nodegroup_name = "dev-node"
  vpc-id = "${module.ja_vpc.VPC_ID[0]}"
  subnet_id = ["${module.ja_vpc.private_subnet_1b_id}", "${module.ja_vpc.private_subnet_1a_id}" ]
  instance_type = "c4.2xlarge"
  instance_disk_size = 100
  instance_ssh_key = "ja-dev"
  eks_desired_size = 2
  eks_min_size = 2
  eks_max_size = 5
}

module "ja_dev_repo" {

   source = "../modules/ecr"
   reponame = "dev-eks"

}

module "ja_dev_acm" {
   source = "../modules/acm/"
   domainname = "joinassembly.com"
   aliasdomainname = "*.joinassembly.com"

}

#module "ja_dev_s3" {
#   source = "../modules/s3"
#   s3_bucket_names = [ "dev-assembly-flows", "dev-assembly-flow-thumbnails", "dev-assembly-temp-download-files" ]

#}

resource "aws_iam_policy" "ALBIngressPolicy" {
    name = "ALBIngressPolicies"
    policy = "${file("albiampolicy.json")}"
}

output "EKSArn" {
   value = aws_iam_policy.ALBIngressPolicy.arn 
 }
